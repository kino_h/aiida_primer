from aiida.orm import Int, Dict
from aiida.engine import calcfunction, WorkChain
from aiida.engine import BaseRestartWorkChain
from aiida.engine import while_
from aiida.engine import process_handler, ProcessHandlerReport


@calcfunction
def add_x_y(x, y):
    return x + y


class IntAddWorkChain(WorkChain):
    """WorkChain to multiply two numbers and add a third, for testing and demonstration purposes."""

    @classmethod
    def define(cls, spec):
        """Specify inputs and outputs."""
        super().define(spec)
        spec.input('x', valid_type=Int)
        spec.input('y', valid_type=Int)
        # spec.input('params', valid_type=Dict, required=False)
        spec.outline(
            # cls.validate_inputs,
            cls.add,
            cls.result,
        )
        spec.output('result', valid_type=Int)

    def add(self):
        """Multiply two integers. returns x*y"""
        self.ctx.sum = add_x_y(self.inputs.x, self.inputs.y)
        print("x,y,sum", self.inputs.x.value, self.inputs.y.value,   self.ctx.sum.value)

    def result(self):
        """Add the result to the outputs."""
        print("result", self.ctx.sum)
        self.out('result', self.ctx.sum)


class PositiveIntAddWorkChain(WorkChain):
    """WorkChain to multiply two numbers and add a third, for testing and demonstration purposes."""

    @classmethod
    def define(cls, spec):
        """Specify inputs and outputs."""
        super().define(spec)
        spec.input('x', valid_type=Int)
        spec.input('y', valid_type=Int)
        spec.input('params', valid_type=Dict, required=False)
        spec.outline(
            # cls.validate_inputs,
            cls.add,
            cls.validate_results,
            cls.result,
        )
        spec.output('result', valid_type=Int)

        # define exit code used in this class.
        # It will be accessed by self.exit_codes.ERROR_NEGATIVE_NUMBER.
        spec.exit_code(400, 'ERROR_NEGATIVE_NUMBER', message='The result is a negative number.')
        spec.exit_code(410, 'ERROR_X_NEGATIVE_NUMBER', message='input x is a negative number.')
        spec.exit_code(420, 'ERROR_Y_NEGATIVE_NUMBER', message='input y is a negative number.')

    def validate_inputs(self):

        x = self.inputs.x
        if x.value < 0:
            return self.exit_codes.ERROR_X_NEGATIVE_NUMBER

        y = self.inputs.y
        if y.value < 0:
            return self.exit_codes.ERROR_Y_NEGATIVE_NUMBER

    def validate_results(self):
        """Make sure the result is not negative."""
        print("validate_results", self.ctx.sum)
        if self.ctx.sum <= 0:
            return self.exit_codes.ERROR_NEGATIVE_NUMBER

    def add(self):
        """Multiply two integers. returns x*y"""
        self.ctx.sum = add_x_y(self.inputs.x, self.inputs.y)
        print("x,y,sum", self.inputs.x.value, self.inputs.y.value,   self.ctx.sum.value)

    def result(self):
        """Add the result to the outputs."""
        print("result", self.ctx.sum)
        self.out('result', self.ctx.sum)


class AddCorrectionBaseWorkChain(BaseRestartWorkChain):

    _process_class = PositiveIntAddWorkChain

    @classmethod
    def define(cls, spec):
        """Define the process specification."""
        super().define(spec)
        #spec.input('x', valid_type=Int)
        #spec.input('y', valid_type=Int)
        #spec.input('params', valid_type=Dict, required=False)
        spec.expose_inputs(cls._process_class)
        #spec.output('result', valid_type=Int)
        spec.expose_outputs(cls._process_class)

        spec.outline(
            cls.setup,
            while_(cls.should_run_process)(
                cls.run_process,
                cls.inspect_process,
            ),
            cls.results,
        )
        spec.exit_code(500, 'ERROR_NO_RECOVERY', message='The product is a negative number.')

    def setup(self):
        """Call the `setup` of the `BaseRestartWorkChain` and then create the inputs dictionary in `self.ctx.inputs`.

        This `self.ctx.inputs` dictionary will be used by the `BaseRestartWorkChain` to submit the process in the
        internal loop.
        """
        super().setup()
        self.ctx.inputs = {'x': self.inputs.x, 'y': self.inputs.y, 'params': self.inputs.params}

    @process_handler(priority=500, exit_codes=_process_class.exit_codes.ERROR_NEGATIVE_NUMBER)
    def handle_negative_result_add_x(self, node):
        """Check if the calculation failed with `ERROR_X_NEGATIVE_NUMBER`.

        If this is the case, simply make the inputs positive by taking the absolute value.

        :param node: the node of the subprocess that was ran in the current iteration.
        :return: optional :class:`~aiida.engine.processes.workchains.utils.ProcessHandlerReport` instance to signal
            that a problem was detected and potentially handled.
        """
        if node.exit_status == self._process_class.exit_codes.ERROR_NEGATIVE_NUMBER.status:
            incx = 0
            incy = node.inputs.params.get_dict()["incy"]

            self.ctx.inputs['x'] = Int(node.inputs.x+incx)
            self.ctx.inputs['y'] = Int(node.inputs.y+incy)
            print("priority 500:", node.inputs.x.value, node.inputs.y.value,
                  "+", incx, incy,
                  "=>", self.ctx.inputs['x'].value, self.ctx.inputs['y'].value)
            return ProcessHandlerReport()

        return ProcessHandlerReport(exit_code=self.exit_codes.ERROR_NO_RECOVERY)

    # The priority 400 is called after the priority 500.
    @process_handler(priority=400, exit_codes=_process_class.exit_codes.ERROR_NEGATIVE_NUMBER)
    def handle_negative_result_add_y(self, node):
        """Check if the calculation failed with `ERROR_X_NEGATIVE_NUMBER`.

        If this is the case, simply make the inputs positive by taking the absolute value.

        :param node: the node of the subprocess that was ran in the current iteration.
        :return: optional :class:`~aiida.engine.processes.workchains.utils.ProcessHandlerReport` instance to signal
            that a problem was detected and potentially handled.
        """
        if node.exit_status == self._process_class.exit_codes.ERROR_NEGATIVE_NUMBER.status:
            incx = node.inputs.params.get_dict()["incx"]
            incy = 0
            self.ctx.inputs['x'] = self.ctx.inputs['x']+incx
            print("priority 400:", self.ctx.inputs['x'].value, self.ctx.inputs['y'].value,
                  "+", incx, incy,
                  "=>", self.ctx.inputs['x'].value, self.ctx.inputs['y'].value)
            return ProcessHandlerReport()

        return ProcessHandlerReport(exit_code=self.exit_codes.ERROR_NO_RECOVERY)
