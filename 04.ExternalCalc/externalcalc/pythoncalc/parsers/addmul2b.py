from aiida.parsers.parser import Parser
from aiida.orm import Float

# ref. https://github.com/aiidateam/aiida-diff/blob/master/aiida_diff/parsers.py

class externalAddMulParser(Parser):

    def parse(self, **kwargs):
        """Parse the contents of the output files stored in the `retrieved` output node."""

        try:
            output_folder = self.retrieved
        except:
            return self.exit_codes.ERROR_NO_RETRIEVED_FOLDER

        outputfilename = self.node.get_option('prog_output_filename')
        print("parser: outputfilename",outputfilename)
        try:
            with output_folder.open(outputfilename, 'r') as handle:
                result = handle.read()
                result = float(result)
                # self.report("read <{}>".format(result)) # no report in Parser!
        except OSError:
            return self.exit_codes.ERROR_READING_OUTPUT_FILE
        except ValueError:
            return self.exit_codes.ERROR_INVALID_OUTPUT

        self.out('result', Float(result))
