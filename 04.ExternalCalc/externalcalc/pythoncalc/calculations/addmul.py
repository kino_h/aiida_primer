from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit,run,while_
from aiida.orm import Str, Float, Int, Dict, load_node, load_code
import numpy as np
from aiida.plugins import DataFactory, CalculationFactory
import time

from aiida.common.datastructures import CalcInfo, CodeInfo
from aiida.common.folders import Folder
from aiida.engine import CalcJob, CalcJobProcessSpec

class externalAddMulCalculation(CalcJob):
    @classmethod
    def define(cls,spec):
        super().define(spec)
        spec.input("x", valid_type=(Int,Float))
        spec.input("y", valid_type=(Int,Float))
        spec.input("z", valid_type=(Int,Float))
        spec.inputs['metadata']['options']['parser_name'].default = 'externalcalc.addmul'
        spec.inputs['metadata']['options']['input_filename'].default = 'aiida.in'
        spec.inputs['metadata']['options']['output_filename'].default = 'aiida.out'
        spec.inputs['metadata']['options']['resources'].default = {'num_machines': 1, 'num_mpiprocs_per_machine': 1}
        spec.output('result', valid_type=Float)

    def prepare_for_submission(self, folder: Folder) -> CalcInfo:
        """Prepare the calculation for submission.

        Convert the input nodes into the corresponding input files in the format that the code will expect. In addition,
        define and return a `CalcInfo` instance, which is a simple data structure that contains information for the
        engine, for example, on what files to copy to the remote machine, what files to retrieve once it has completed,
        specific scheduler settings and more.

        :param folder: a temporary folder on the local file system.
        :returns: the `CalcInfo` instance
        """
        values = []
        for v in [self.inputs.x, self.inputs.y, self.inputs.z]:
            values.append(str(v.value))
        self.report("values {}".format(".".join(values)))
        with folder.open(self.options.input_filename, 'w', encoding='utf8') as handle:
            handle.write("\n".join(values))

        codeinfo = CodeInfo()
        codeinfo.code_uuid = self.inputs.code.uuid
        codeinfo.stdin_name = self.options.input_filename
        codeinfo.stdout_name = self.options.output_filename

        calcinfo = CalcInfo()
        calcinfo.codes_info = [codeinfo]
        calcinfo.retrieve_list = [self.options.output_filename]

        return calcinfo        
    
