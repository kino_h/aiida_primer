from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit,run,while_
from aiida.orm import Str, Float, Int, Dict, Bool, load_node, load_code
import numpy as np
from aiida.plugins import DataFactory, CalculationFactory
import time

from aiida.common.datastructures import CalcInfo, CodeInfo
from aiida.common.folders import Folder
from aiida.engine import CalcJob, CalcJobProcessSpec

class externalAddMulCalculation(CalcJob):

    _OUTPUT_PROG = "Outputfile.txt"
    _WITHMPI = True

    @classmethod
    def define(cls,spec):
        super().define(spec)

        #spec.inputs['metadata']['options']['resources'].default = {
        #    'num_machines': 1,
        #    'num_mpiprocs_per_machine': 1,
        #}
        spec.input("x", valid_type=(Int,Float))
        spec.input("y", valid_type=(Int,Float))
        spec.input("z", valid_type=(Int,Float))
        spec.input("input_filename", valid_type=Str)
        #spec.input('prog_output_filename', valid_type=Str, default=lambda: Str(cls._OUTPUT_PROG))
        spec.input('metadata.options.prog_output_filename', valid_type=str, default=cls._OUTPUT_PROG)
        spec.input('metadata.options.withmpi', valid_type=bool, default=cls._WITHMPI)
        spec.inputs['metadata']['options']['parser_name'].default = 'externalcalc.addmul2b'
        spec.inputs['metadata']['options']['output_filename'].default = 'aiida.out'
        spec.inputs['metadata']['options']['resources'].default = {'num_machines': 1, 'num_mpiprocs_per_machine': 1}
        spec.output('result', valid_type=Float)

    def prepare_for_submission(self, folder: Folder) -> CalcInfo:
        """Prepare the calculation for submission.

        Convert the input nodes into the corresponding input files in the format that the code will expect. In addition,
        define and return a `CalcInfo` instance, which is a simple data structure that contains information for the
        engine, for example, on what files to copy to the remote machine, what files to retrieve once it has completed,
        specific scheduler settings and more.

        :param folder: a temporary folder on the local file system.
        :returns: the `CalcInfo` instance
        """
        values = []
        for v in [self.inputs.x, self.inputs.y, self.inputs.z]:
            values.append(str(v.value))
        self.report("values {}".format(".".join(values)))
        # self.inputs.input_filename is Str
        input_filename = self.inputs.input_filename.value
        print("Input_filename",input_filename)
        with folder.open(input_filename, 'w', encoding='utf8') as handle:
            handle.write("\n".join(values))

        codeinfo = CodeInfo()
        codeinfo.code_uuid = self.inputs.code.uuid
        # codeinfo.stdin_name = self.options.input_filename
        codeinfo.stdout_name = self.options.output_filename
        # remember that self.inputs.input_filename is Str
        codeinfo.cmdline_params = [ self.inputs.input_filename.value ]
        # the content of metadata isn't AiiDA.orm
        # default value of withmpi=False
        #codeinfo.withmpi = self.inputs.metadata.options.withmpi
        codeinfo.withmpi = self.options.withmpi

        calcinfo = CalcInfo()
        calcinfo.codes_info = [codeinfo]
        # self.options.prog_output_filename is str. 
        calcinfo.retrieve_list = [self.options.output_filename, self.options.prog_output_filename]

        return calcinfo        
    
