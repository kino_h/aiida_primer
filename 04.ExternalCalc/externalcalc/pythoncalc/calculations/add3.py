from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit,run,while_
from aiida.orm import Str, Float, Int, Dict, load_node, load_code
import numpy as np
from aiida.plugins import DataFactory, CalculationFactory
import time

from aiida.common.datastructures import CalcInfo, CodeInfo
from aiida.common.folders import Folder
from aiida.engine import CalcJob, CalcJobProcessSpec

class externalAddCalculation(CalcJob):


    @classmethod
    def define(cls,spec):
        super().define(spec)
        spec.input("x", valid_type=(Int,Float))
        spec.input("y", valid_type=(Int,Float))
        spec.input("arg_input_filename", valid_type=Str)
        spec.input('arg_output_filename', valid_type=Str)
        #spec.input('prog_output_filename', valid_type=Str, default=lambda: Str(cls._OUTPUT_PROG))
        spec.inputs['metadata']['options']['parser_name'].default = 'externalcalc.add3'
        spec.inputs['metadata']['options']['output_filename'].default = 'aiida.out'
        spec.inputs['metadata']['options']['resources'].default = {'num_machines': 1, 
                'num_mpiprocs_per_machine': 1}
        spec.output('result', valid_type=Float)

    def prepare_for_submission(self, folder: Folder) -> CalcInfo:
        """Prepare the calculation for submission.

        Convert the input nodes into the corresponding input files in the format that the code will expect. In addition,
        define and return a `CalcInfo` instance, which is a simple data structure that contains information for the
        engine, for example, on what files to copy to the remote machine, what files to retrieve once it has completed,
        specific scheduler settings and more.

        :param folder: a temporary folder on the local file system.
        :returns: the `CalcInfo` instance
        """
        values = []
        for v in [self.inputs.x, self.inputs.y]:
            values.append(str(v.value))
        self.report("values {}".format(".".join(values)))
        # self.inputs.input_filename is Str
        input_filename = self.inputs.arg_input_filename.value
        print("Input_filename",input_filename)
        with folder.open(input_filename, 'w', encoding='utf8') as handle:
            handle.write("\n".join(values))

        codeinfo = CodeInfo()
        codeinfo.code_uuid = self.inputs.code.uuid
        # codeinfo.stdin_name = self.options.input_filename
        codeinfo.stdout_name = self.options.output_filename
        # remember that self.inputs.input_filename is Str
        codeinfo.cmdline_params = [ self.inputs.arg_input_filename.value, 
                                    self.inputs.arg_output_filename.value]
        # codeinfo.withmpi = True if MPI

        calcinfo = CalcInfo()
        calcinfo.codes_info = [codeinfo]
        # See type definition in self.metadata.options.output_filename 
        # self.options is the same as self.metadata.options
        #calcinfo.retrieve_list = [self.options.output_filename, self.metadata.options.prog_output_filename]
        calcinfo.retrieve_list = [self.inputs.arg_input_filename.value, 
                self.inputs.arg_output_filename.value, 
                self.options.output_filename]
        print("calcinfo.retrieve_list", calcinfo.retrieve_list)

        return calcinfo        
    
