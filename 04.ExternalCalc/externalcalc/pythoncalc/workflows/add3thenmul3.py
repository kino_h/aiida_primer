
from aiida.plugins import DataFactory
import aiida
from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit,run,while_
from aiida.orm import Str, Float, Int, Dict, load_node, load_code
import numpy as np
from aiida.plugins import DataFactory, CalculationFactory
import time

from aiida.common.datastructures import CalcInfo, CodeInfo
from aiida.common.folders import Folder
from aiida.engine import CalcJob, CalcJobProcessSpec

class Add3ThenMul3WorkChain(WorkChain):

    @classmethod
    def define(cls, spec):
        super().define(spec)
        spec.input('x')
        spec.input('y')
        spec.input('z')
        spec.outline(
            cls.call_add3,
            cls.call_mul3,
            cls.results,
        )
        spec.output('result')

    def call_add3(self):
        code = load_code("externalcalc.add3@tutor")
        builder = code.get_builder()
        builder.x = self.inputs.x
        builder.y = self.inputs.y
        builder.arg_input_filename =  Str("input.txt")
        builder.arg_output_filename =  Str("output.txt")
        future = self.submit(builder) # changed
        return ToContext(workchain=future) # changed

    def call_mul3(self):
        code = load_code("externalcalc.mul3@tutor")
        builder = code.get_builder()
        builder.x = self.ctx.workchain.outputs.result # changed
        builder.y = self.inputs.z
        builder.arg_input_filename =  Str("input2a.txt")
        builder.arg_input_filename2 =  Str("input2b.txt")
        builder.arg_output_filename =  Str("output2.txt")

        future = self.submit(builder)  # changed
        return ToContext(workchain=future)  # changed

    def results(self):
        result = self.ctx.workchain.outputs.result # changed
        self.out('result', result) 
        
