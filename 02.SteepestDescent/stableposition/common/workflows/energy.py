from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit, run, while_

from aiida.orm import Str,Float
import numpy as np
from aiida.plugins import DataFactory

@calcfunction
def _energy(x):
    return Float(x**2)

class EnergyChain(WorkChain):
    @classmethod
    def define(cls,spec):
        super().define(spec)
        spec.input("x", valid_type=Float)
        spec.outline(cls.energy)
        spec.output("energy",valid_type=Float)

    def energy(self):
        x = self.inputs.x
        y = _energy(x)
        self.out("energy", y)

