from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit, run, while_

from aiida.orm import Str,Float
import numpy as np
from aiida.plugins import DataFactory

@calcfunction
def _new_x(x,f,a):
    return Float(x+a*f)

class NewXChain(WorkChain):
    @classmethod
    def define(cls,spec):
        super().define(spec)
        spec.input("x", valid_type=Float)
        spec.input("f", valid_type=Float)
        spec.input("a", valid_type=Float)
        spec.outline(cls.new_x)
        spec.output("new_x",valid_type=Float)

    def new_x(self):
        x = self.inputs.x
        f = self.inputs.f
        a = self.inputs.a
        y = _new_x(x,f,a)
        self.out("new_x", y)
