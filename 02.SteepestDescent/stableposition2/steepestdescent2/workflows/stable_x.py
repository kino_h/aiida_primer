from aiida.plugins import WorkflowFactory
from aiida.orm import Float, load_node
from aiida.engine import run,submit,workfunction, WorkChain, ToContext, while_
import numpy as np
import time

class StableXChain(WorkChain):
    @classmethod
    def define(cls,spec):
        super().define(spec)
        spec.input("x", valid_type=Float)
        spec.input("a", valid_type=Float)
        spec.input("fth", valid_type=Float)
        spec.outline(cls.initialize,
                     cls.force,
                   while_ (cls.not_converged)(
                   cls.new_x,
                   cls.force
                   ),
                    cls.finalize)
        spec.output("stable_x", valid_type=Float)
        spec.output("stable_force", valid_type=Float)

    def initialize(self):
        # internal variable
        self.ctx._iteration = 0
        self.ctx._iteration_max = 100
        
    def force(self):
        if "new_x" in self.ctx:
            x = self.ctx.new_x.outputs.new_x
        else:
            x = self.inputs.x
        forcechain = WorkflowFactory("stablex.force")
        force_builder = forcechain.get_builder()
        force_builder.x = x
        # run() doesn't work here.
        # submit() must be used. 
        future = self.submit(force_builder)
        self.to_context(**{"force": future})
        
    def not_converged(self):
        f = self.ctx.force.outputs.force
        fth = self.inputs.fth
        iteration = self.ctx._iteration
        print("i,f,fth",iteration, f.value, fth.value)
        return np.abs(f.value) > fth.value and self.ctx._iteration < self.ctx._iteration_max

    def new_x(self):
        if "new_x" in self.ctx:
            x = self.ctx.new_x.outputs.new_x
        else:
            x = self.inputs.x
        f = self.ctx.force.outputs.force
        a = self.inputs.a
        newxchain = WorkflowFactory("stablex.sd_new_x")
        newx_builder = newxchain.get_builder()
        newx_builder.x = x
        newx_builder.f = f
        newx_builder.a = a
        future = self.submit(newx_builder)
        self.to_context(**{"new_x": future})
        self.ctx._iteration += 1
        
    def finalize(self):
        if "new_x" in self.ctx:
            x = self.ctx.new_x.outputs.new_x
        else:
            x = self.inputs.x    
        self.out("stable_x",x)
        f = self.ctx.force.outputs.force
        self.out("stable_force",f)

