from aiida.engine import calcfunction,workfunction, WorkChain, ToContext, submit, run, while_

from aiida.orm import Str,Float
import numpy as np
from aiida.plugins import DataFactory

@calcfunction
def _force(x):
    return Float(-2.0*x)

class ForceChain(WorkChain):
    @classmethod
    def define(cls,spec):
        super().define(spec)
        spec.input("x", valid_type=Float)
        spec.outline(cls.force)
        spec.output("force",valid_type=Float)

    def force(self):
        x = self.inputs.x
        y = _force(x)
        self.out("force", y)

