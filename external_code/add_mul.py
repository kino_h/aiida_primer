#!/home/kino/miniconda3/envs/aiida/bin/python
import sys
import numpy as np

if __name__ == "__main__":
    lines = sys.stdin.readlines()
    nums =[]
    for line in lines:
        i = float(line)
        nums.append(i)
    nums = np.array(nums)
    i1 = nums[0]
    i2 = nums[1]
    i3 = nums[2]
    result = (i1+i2)*i3
    print(result)
    sys.exit(0)
