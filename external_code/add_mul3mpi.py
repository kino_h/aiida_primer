#!/home/kino/miniconda3/envs/aiida/bin/python

import sys
import numpy as np
from mpi4py import MPI

if __name__ == "__main__":
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    print("rank, size", rank,size)
    comm.Barrier()
    if rank==0:
        inputfile = sys.argv[1]
        outputfile = "Outputfile.txt"
        print("inputfile",inputfile)
        with open(inputfile) as f:
            lines = f.readlines()
        nums =[]
        for line in lines:
            i = float(line)
            nums.append(i)
        nums = np.array(nums)
        print("nums",nums)
        i1 = nums[0]
        i2 = nums[1]
        i3 = nums[2]
        result = (i1+i2)*i3
        print("outputfile",outputfile)
        with open(outputfile,"w") as f:
            f.write(str(result))
        print("result",result)
    sys.exit(0)
