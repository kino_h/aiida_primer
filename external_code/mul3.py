#!/home/kino/miniconda3/envs/aiida/bin/python

import sys
import numpy as np

def readfloat(inputfile):
    print("inputfile",inputfile)
    with open(inputfile) as f:
        lines = f.readlines()
    nums =[]
    for line in lines:
        i = float(line)
        nums.append(i)
    nums = np.array(nums)
    print("nums",nums)
    return nums


if __name__ == "__main__":
    inputfile1 = sys.argv[1]
    inputfile2 = sys.argv[2]
    outputfile = sys.argv[3]
    nums = readfloat(inputfile1)
    i1 = nums[0]
    nums = readfloat(inputfile2)
    i2 = nums[0]
    result = i1*i2
    print("outputfile",outputfile)
    with open(outputfile,"w") as f:
        f.write(str(result))
    print("result",result)
    sys.exit(0)
