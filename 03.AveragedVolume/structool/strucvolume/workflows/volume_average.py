from aiida.engine import (
    calcfunction,
    workfunction,
    WorkChain,
    ToContext,
    submit,
    run,
    while_,
)

from aiida.orm import Str, Float, Dict
import numpy as np
from aiida.plugins import DataFactory


@calcfunction
def _volume_average(calculations):
    print("_volume_average.calculations", calculations)
    volume_list = []
    for label, value in calculations.get_dict().items():
        volume_list.append(value)
    volume_avg = np.average(volume_list)
    print("volume_avg", volume_avg)
    return Float(volume_avg)


class VolumeAverage(WorkChain):
    @classmethod
    def define(cls, spec):
        super().define(spec)
        StructureData = DataFactory("core.structure")
        spec.input("volumes", valid_type=Dict)
        spec.outline(cls.volume_average)
        spec.output("volume", valid_type=Float)

    def volume_average(self):
        volumes = self.inputs.volumes
        volume = _volume_average(volumes)
        self.out("volume", volume)
