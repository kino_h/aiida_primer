from aiida.engine import (
    calcfunction,
    workfunction,
    WorkChain,
    ToContext,
    submit,
    run,
    while_,
)
from aiida.orm import Str, Float, Dict, load_node
import numpy as np
from aiida.plugins import DataFactory
from aiida.plugins import WorkflowFactory

import time


@calcfunction
def _load_poscar(element, absolute_path):
    import os

    symbol = element.value
    directory = absolute_path.value
    from pymatgen.core.structure import Structure

    filename = "POSCAR." + symbol
    path = os.path.join(directory, filename)
    struc = Structure.from_file(path)
    StructureData = DataFactory("core.structure")
    structure = StructureData()
    structure.set_pymatgen(struc)
    return structure


@calcfunction
def _rescale_volume(structure, scale):
    """
    Workfunction to rescale a structure

    :param structure: An AiiDA structure to rescale
    :param scale: The scale factor (for the lattice constant)
    :return: The rescaled structure
    """
    the_ase = structure.get_ase()
    new_ase = the_ase.copy()
    new_ase.set_cell(the_ase.get_cell() * scale.value, scale_atoms=True)
    new_structure = DataFactory("core.structure")(ase=new_ase)

    struc = new_structure.get_pymatgen()
    return Float(struc.volume)


# This subroutine is important.
@calcfunction
def _pack_calculations(**kwargs):
    print("pack", kwargs)
    result = [result for label, result in kwargs.items()]
    return Dict(dict={"volumes": result})


@calcfunction
def _volume_average(calculations):
    print("_volume_average.calculations", calculations)
    volumes = calculations["volumes"]
    volume_avg = np.average(volumes)
    print("volume_avg", volume_avg)
    return Float(volume_avg)


@calcfunction
def _average_volumes(**kwargs):
    print("pack", kwargs)
    volumes = [result.value for label, result in kwargs.items()]
    print("volumes", volumes)
    volume_avg = np.average(volumes)
    print("volume_avg", volume_avg)
    return Float(volume_avg)


class RescaledStructuresVolume(WorkChain):
    @classmethod
    def define(cls, spec):
        super().define(spec)

        spec.input("element", valid_type=Str)
        ArrayData = DataFactory("core.array")
        spec.input("factor_array", valid_type=ArrayData)
        spec.input("absolute_path", valid_type=Str)
        spec.outline(
            cls.create_diamond_fcc,
            cls.run_rescale_volume,
            cls.volume_agv,
        )
        spec.output("volume_avg", valid_type=Float)

    def create_diamond_fcc(self):
        element = self.inputs.element
        factor_array = self.inputs.factor_array
        absolute_path = self.inputs.absolute_path
        s0 = _load_poscar(element, absolute_path)
        # Carefully connect the intermediate data using self.ctx.
        self.ctx.s0 = s0

    def run_rescale_volume(self):
        structure = self.ctx.s0
        calculations = {}
        factor_array = self.inputs.factor_array
        factors = factor_array.get_array("vector")
        for i, factor in enumerate(factors):
            label = "iter{}".format(i)
            new_volume = _rescale_volume(structure, Float(factor))
            print(label, "new_volume", new_volume)
            calculations[label] = new_volume
        print("calculation", calculations)
        inputs = {label: result for label, result in calculations.items()}
        self.ctx.calculations = _pack_calculations(**inputs)

    def volume_agv(self):
        calculations = self.ctx.calculations
        print("calculations2", calculations)
        result = _volume_average(calculations)
        print("result", result)
        # output is set by self.out()
        self.out("volume_avg", result)
