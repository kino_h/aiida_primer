from aiida.engine import (
    calcfunction,
    workfunction,
    WorkChain,
    ToContext,
    submit,
    run,
    while_,
)

from aiida.orm import Str, Float
import numpy as np
from aiida.plugins import DataFactory
import time


@calcfunction
def _rescale_volume(structure, scale):
    """
    Workfunction to rescale a structure

    :param structure: An AiiDA structure to rescale
    :param scale: The scale factor (for the lattice constant)
    :return: The rescaled structure
    """
    the_ase = structure.get_ase()
    new_ase = the_ase.copy()
    new_ase.set_cell(the_ase.get_cell() * scale.value, scale_atoms=True)
    new_structure = DataFactory("core.structure")(ase=new_ase)
    struc = new_structure.get_pymatgen()
    return Float(struc.volume)


class RescaleVolume(WorkChain):
    @classmethod
    def define(cls, spec):
        super().define(spec)
        StructureData = DataFactory("core.structure")
        spec.input("structure", valid_type=StructureData)
        spec.input("scale", valid_type=Float)
        spec.outline(cls.rescale)
        spec.output("scaled_volume", valid_type=Float)

    def rescale(self):
        # wait_sec = 10
        # self.report("RescaleVolume start. wait for {} sec.".format(wait_sec))
        # time.sleep(wait_sec)
        structure = self.inputs.structure
        scale = self.inputs.scale
        rescaled_volume = _rescale_volume(structure, Float(scale))
        self.out("scaled_volume", rescaled_volume)
