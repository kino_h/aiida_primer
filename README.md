# AiiDA primer #

* AiiDA primer
* version 0.3

以下で動作確認。
```
aiida-core                    2.6.2
aiida-dataframe               0.1.3
```
 
10.vaspはAiiDA ver. 2になってから動作確認していない。

## License

Copyright (c) 2021-2024, Hiori Kino

All rights reserved.

This software is released under the BSD License (3-clause BSD License).

