{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5acda5ca",
   "metadata": {},
   "source": [
    "# A real world workchain example: electronic band structure\n",
    "\n",
    "Let us start with some preliminary imports.\n",
    "\n",
    "Before starting to run, make sure that you are using the `quicksetup` profile as the default profile!\n",
    "You can check which is the default profile using `verdi profile list` (it will be the one with a `*` in front).\n",
    "\n",
    "If it is not the default profile, use `verdi profile setdefault quicksetup`, and then restart your kernel before running the following lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c62bd2b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "%aiida\n",
    "\n",
    "from datetime import datetime, timedelta\n",
    "from aiida.engine import run\n",
    "from aiida.plugins import DbImporterFactory\n",
    "CodDbImporter = DbImporterFactory('cod')\n",
    "\n",
    "PwBandStructureWorkChain = WorkflowFactory('quantumespresso.pw.band_structure')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dab8998",
   "metadata": {},
   "source": [
    "## Calculating the electronic band structure with an AiiDA workchain\n",
    "\n",
    "This tutorial will show how useful a workchain can be in performing a well defined task, such as computing and visualizing the electronic band structure for a simple crystal structure.\n",
    "The goal of this tutorial is not to show you the intricacies of the actual workchain itself, but rather to serve as an example that workchains can simplify standard workflows in computational materials science.\n",
    "The workchain that we will use here will employ Quantum ESPRESSO's `pw.x` code to calculate the charge densities for several crystal structures and compute a band structure from those.\n",
    "Many choices that normally face the researcher before being able to perform this calculation, such as the selection of suitable pseudo potentials, energy cutoff values, k-point grids and k-point paths along high symmetry points, are now performed automatically by the workchain.\n",
    "All that remains for the user to do is to simply define a structure, pass it to the workchain and sit back!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "709a8c92",
   "metadata": {},
   "source": [
    "Below, we import the crystal structure of Al (aluminium) as an example, and run the `PwBandStructureWorkChain` for that structure.\n",
    "The estimated run time is noted in a comment in the calculation cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2874f078",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Loading the COD importer so we can directly import structure from COD id's\n",
    "importer = CodDbImporter()\n",
    "# Make sure here to define the correct codename that corresponds to the pw.x code installed on your machine of choice\n",
    "codename = 'qe-6.5-pw@localhost'\n",
    "code = Code.get_from_string(codename)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e056cbd",
   "metadata": {},
   "source": [
    "### Importing example crystal structures from COD to AiiDA structure objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "defad9d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Al COD ID='9008460'\n",
    "structure_Al = importer.query(id='9008460')[0].get_aiida_structure()\n",
    "\n",
    "structure_Al.get_formula()\n",
    "\n",
    "# The following structure can be used instead of Al, but will take much longer on the AWS machine.\n",
    "# CaF2 COD ID='1000043' -- approximately 1/2 hour to run\n",
    "# h-BN COD ID='9008997' -- approximately 45 mins to run\n",
    "# GaAs COD ID='9008845' -- approximately 2 hours to run"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81695744",
   "metadata": {},
   "source": [
    "### Now we run the bandstructure workchain for the selected structures"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f24b2eba",
   "metadata": {},
   "source": [
    "The bandstructure workchain follows the following protocol:\n",
    "\n",
    "* Determine the primitive cell of the input structure\n",
    "* Run a `vc-relax` (variable-cell) computation, to relax the structure\n",
    "* Refine the symmetry of the relaxed structure, to ensure the primitive cell is used, and run a self-consistent field calculation on it\n",
    "* Run a non self-consistent field band structure calculation, along a path of high symmetry k-points determined by [seekpath](http://materialscloud.org/tools/seekpath)\n",
    "\n",
    "Numerical parameters for the default 'theos-ht-1.0' protocol are determined as follows:\n",
    "\n",
    "* Suitable pseudopotentials and energy cutoffs are automatically searched from the [SSSP library](http://materialscloud.org/sssp) installed on your machine  (it uses the efficiency version 1.1).\n",
    "* K-point mesh is selected to have a minimum k-point density of 0.2 Å<sup>-1</sup>\n",
    "* A Marzari-Vanderbilt smearing of 0.02 Ry is used for the electronic occupations\n",
    "\n",
    "In case the pseudopotentials are not installed, they can be downloaded in a terminal as:\n",
    "\n",
    "```console\n",
    "$ mkdir sssp_pseudos\n",
    "$ wget 'https://archive.materialscloud.org/record/file?filename=SSSP_1.1_PBE_efficiency.tar.gz&record_id=23&file_id=d2ce4186-bf76-4e05-8b39-444b4da30273' -O SSSP_1.1_PBE_efficiency.tar.gz\n",
    "$ tar -C sssp_pseudos -zxvf SSSP_1.1_PBE_efficiency.tar.gz\n",
    "$ verdi data upf uploadfamily sssp_pseudos 'SSSP' 'SSSP pseudopotential library'\n",
    "```\n",
    "\n",
    "The protocol looks for a UPF file with a specific hash code, that is unique for each different file.\n",
    "You can check that you have the right\n",
    "one by performing a search in the database:\n",
    "\n",
    "```python\n",
    "UpfData = DataFactory('upf')\n",
    "qb=QueryBuilder()\n",
    "qb.append(UpfData, filters={'attributes.md5':{'==':'cfc449ca30b5f3223ec38ddd88ac046d'}})\n",
    "len(qb.all())\n",
    "```\n",
    "\n",
    "'md5' is a searchable attribute of the pseudopotential data object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "361d5fdc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This will take approximately 6 minutes on the tutorial AWS (for Al)\n",
    "results = run(\n",
    "    PwBandStructureWorkChain,\n",
    "    code=code,\n",
    "    structure=structure_Al\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5ebb6db",
   "metadata": {},
   "outputs": [],
   "source": [
    "fermi_energy = results['scf_parameters'].dict.fermi_energy\n",
    "results['band_structure'].show_mpl(y_origin=fermi_energy, plot_zero_axis=True)\n",
    "\n",
    "print(\"\"\"Final crystal symmetry: {spacegroup_international} (number {spacegroup_number})\n",
    "Extended Bravais lattice symbol: {bravais_lattice_extended}\n",
    "The system has inversion symmetry: {has_inversion_symmetry}\"\"\".format(\n",
    "    **results['seekpath_parameters'].get_dict()))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8636651a",
   "metadata": {},
   "source": [
    "If you want to use a different pseudopotential family (or version of the family) (for instance [SSSP v1.0](https://doi.org/10.24435/materialscloud:2018.0001/v1) instead of the default SSSP v1.1) you can pass an additional parameter when calling the WorkChain, as follows:\n",
    "\n",
    "```python\n",
    "    run(\n",
    "        # ...,\n",
    "        protocol = Dict(dict={\n",
    "           'name':'theos-ht-1.0',\n",
    "           'modifiers': {\n",
    "           'pseudo' : 'SSSP-efficiency-1.0'\n",
    "           }\n",
    "        })\n",
    "    )\n",
    "```\n",
    "\n",
    "```{note}\n",
    "Only some values are accepted for pseudo, that you can find [here](https://github.com/aiidateam/aiida-quantumespresso/blob/a52266d096afe48dbc6b38b63ac17a9989dd12fe/aiida_quantumespresso/utils/protocols/pw.py#L24); and that you will have to import the pseudopotentials in AiiDA first.\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "text_representation": {
    "extension": ".md",
    "format_name": "myst",
    "format_version": "0.9",
    "jupytext_version": "1.5.1"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
