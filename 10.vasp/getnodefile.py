from aiida.orm import load_node
from aiida.repository import FileType
import os
import sys
from shutil import copyfileobj

def copy_calcjob_file(action, calcjob, path, outputdir):
    pk = calcjob.pk
    if action in ["inputls", "outputls"]:
        cmd = "verdi calcjob {} {}".format(action, pk)
    elif action in ["inputcat", "outputcat"] and path is not None:
        cmd = "verdi calcjob {} {} {}".format(action, pk, path)
    else:
        raise TypeError
    proc = subprocess.run(cmd, shell=True, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    # return default
    print(cmd)

    ret = 1
    if proc.returncode==0:
        outputfile = os.path.join(outputdir,path)
        with open(outputfile,"wb") as f:
            f.write(proc.stdout)
        ret = 0
    else:
        print(proc.stderr.decode("utf8"))
    return ret



class CopyInputOutputFiles:
    def __init__(self, inputfilelist, outputfilelist, method="direct"):
        self.inputfilelist = inputfilelist
        self.outputfilelist = outputfilelist    
        self.method = method
        
    def mkdir(self, outputdir):
        if not os.path.isdir(outputdir):
            os.mkdir(outputdir)     

    def copy(self, calcjob,  outputdir, debugprint=False):
        if self.method == "direct":
            return self.copy_input_and_output_files_directly(calcjob,  outputdir, debugprint=False)
        elif self.method == "verdi":
            return self.copy_input_and_output_files_verdi(calcjob,  outputdir, debugprint=False)\
            
    def copy_input_and_output_files_directly(self, calcjob,  outputdir, debugprint=False):

        self.mkdir(outputdir)
        
        inputfilelist = self.inputfilelist
        outputfilelist = self.outputfilelist
        
        # input side
        for filename in inputfilelist:
            outputfilename = os.path.join(outputdir, filename)
            if debugprint:
                print(outputfilename)
            with open(outputfilename,"wb") as fout: #don't change the order of opening files.
                with calcjob.open(filename, mode='rb') as fhandle:
                    copyfileobj(fhandle, fout)

        # output side
        try:
            retrieved = calcjob.outputs.retrieved
        except AttributeError:
            print("No 'retrieved' node found. Have the calcjob files already been retrieved?")
            return 1

        for filename in outputfilelist:
            outputfilename = os.path.join(outputdir, filename)
            if debugprint:
                print(outputfilename)
            with open(outputfilename,"wb") as fout: #don't change the order of opening files.
                with retrieved.open(filename, mode='rb') as fhandle:
                    copyfileobj(fhandle, fout)
        return 0
    
    def copy_input_and_output_files_verdi(self, calcjob, outputdir, debugprint=False):
        self.mkdir(outputdir)

        inputfilelist = self.inputfilelist
        outputfilelist = self.outputfilelist
        
        pk = calcjob.pk
        
        # input side
        for filename in inputfilelist:
            if debugprint:
                print(filename)
            ret  = copy_calcjob_file("inputcat",calcjob,filename, outputdir)
            if debugprint:
                print(ret,"for",filename)
            if ret !=0:
                raise FileNotFoundError

        # output side
        for filename in outputfilelist:
            if debugprint:
                print(filename)
            ret  = copy_calcjob_file("outputcat",calcjob,filename, outputdir)
            if debugprint:
                print(ret,"for", filename)
            if ret !=0:
                raise FileNotFoundError

        return 0

